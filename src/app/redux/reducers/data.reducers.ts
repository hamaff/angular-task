import { Action } from '@ngrx/store';

import staticData from '../../helpers/data.json';
import { StaticData } from '../../helpers/data.model';
import * as fromActions from '../actions/data.actions';

export const initialState: StaticData[] = [{
    id: null,
    date: null,
    parent: null,
}];

export function DataReducer(state = initialState, action: Action) {
    switch (action.type) {
        case fromActions.DATA_LOADED: {
            return staticData;
        }

        default:
            return state;
    }
}
