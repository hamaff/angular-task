import { createFeatureSelector } from '@ngrx/store';

import { StaticData } from '../../helpers/data.model';
import { DataReducer } from './data.reducers';

export interface DataReducerState {
    staticData: StaticData;
}

export const dataReducer = {
    staticData: DataReducer,
};

export const dataSelector = createFeatureSelector<DataReducerState>('static-data');
