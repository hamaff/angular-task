import { Action } from '@ngrx/store';

export const DATA_LOADED = '[Static Data] Loaded Successful';

export class DataLoaded implements Action {
    readonly type = DATA_LOADED;
}
