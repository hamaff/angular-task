import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DataComponent } from './data-component/data.component';
import { ParentComponent } from './parent-component/parent.component';
import { StoreModule } from '@ngrx/store';
import { dataReducer } from './redux/reducers';

@NgModule({
  declarations: [
    AppComponent,
    DataComponent,
    ParentComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot(dataReducer),
    StoreModule.forFeature('static-data', dataReducer),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
