import { Component, Input } from '@angular/core';

@Component({
    selector: 'data-component',
    templateUrl: './data.component.html',
})

export class DataComponent {
    @Input() data = [];
}
