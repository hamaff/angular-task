import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';

import { ParentComponent } from './parent.component';
import staticData from '../helpers/data.json';

export class MockStore {
    select(el): Observable<Object> {
        return of(null);
    }

    dispatch(param): void {
        return;
    }
}

describe('ParentComponent', () => {
    let component: ParentComponent;
    let fixture: ComponentFixture<ParentComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ParentComponent],
            schemas: [NO_ERRORS_SCHEMA],
            providers: [{
                provide: Store,
                useClass: MockStore
            }]
        });
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ParentComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component)
            .toBeDefined();
    });

    it('should load table data', async(() => {
        component.onClick(staticData[1]);

        expect(component.dataState)
            .toBeDefined();
    }));
});
