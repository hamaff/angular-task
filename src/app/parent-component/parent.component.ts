import { Component, OnInit } from '@angular/core';
import { createSelector, Store } from '@ngrx/store';
import { take } from 'rxjs/operators';

import { StaticData } from '../helpers/data.model';
import * as fromActions from '../redux/actions/data.actions';
import { DataReducerState, dataSelector } from '../redux/reducers';

@Component({
  selector: 'parent-component',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss']
})
export class ParentComponent implements OnInit {
    dataState: StaticData;
    dataDetails: StaticData;

    private storeSelector = createSelector(dataSelector, (state: DataReducerState) => state.staticData);

    constructor(
        private store: Store<DataReducerState>,
    ) {}

    onClick(data): void {
        this.dataDetails = data;
    }

    ngOnInit(): void {
        // dispatch action for redux store
        this.store.dispatch(new fromActions.DataLoaded());

        // subscribe to redux store and load static data
        const subscriber = this.store.select(this.storeSelector)
            .pipe(
                take(1)
            )
            .subscribe((state: StaticData) => {
                this.dataState = state;
            });
    }
}
